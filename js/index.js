
    $(function () { $('[data-toggle="tooltip"]').tooltip()});
    $(function () { $('[data-toggle="popover"]').popover()});
    $('.carousel').carousel({interval: 3000});
    $(function(){
        $('#SOS').on('show.bs.modal', function (e) { 
            console.log ('Modal is coming');
            $('#SOSbtn').removeClass('btn-es');
            $('#SOSbtn').addClass('btn btn-secondary btn-block');
            $('#SOSbtn').prop('disabled',true);
        });
        $('#SOS').on('shown.bs.modal', function (e) { console.log ('Hello Modal')});
        $('#SOS').on('hide.bs.modal', function (e) { console.log ('Goodbye Modal')});
        $('#SOS').on('hidden.bs.modal', function (e) { 
            console.log ('Modal has gone');
            $('#SOSbtn').prop('disabled',false);
            $('#SOSbtn').removeClass('btn btn-secondary btn-block');
            $('#SOSbtn').addClass('btn-es');        
        });
    });    